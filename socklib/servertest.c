/*
 * servertest - test socketlib server functionality
 *
 * Copyright (c) 2003 port GmbH Halle (Saale)
 *------------------------------------------------------------------
 * $Header: /z2/cvsroot/projects/00262/software/servertest.c,v 1.2 2003/10/16 13:05:06 boe Exp $
 *
 *------------------------------------------------------------------
 *
 * modification history
 * --------------------
 * $Log: servertest.c,v $
 * Revision 1.2  2003/10/16 13:05:06  boe
 * use defines for returnvalues from so_server_doit
 *
 * Revision 1.1  2003/10/16 09:01:28  boe
 * testfile for using server from socklib
 *
 *
 *
 *------------------------------------------------------------------
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#if defined( __WIN32__) || defined(_WIN32)
# ifndef __WIN32__
#  define __WIN32__
# endif
#  include <io.h>
#  include <sys/types.h>
#  include <winsock.h>
#  include <string.h>
#  include <errno.h>

#else
/* LINUX || CYGWIN */
#  include <unistd.h>                                            
#  include <errno.h>
#  include <signal.h>
#  include <sys/socket.h>
#  include <netdb.h>
#  include <netinet/in.h>
#  include <strings.h>
#endif


#define S_LIBRARY
#include "socklib.h"

#define MAX_CLIENTS		2

#define MAXLINE 100
char buffer[MAXLINE];		/* command input line from socket to m4d */

extern int	so_debug;

int main()
{
SOCKET_T *listen_fd;		/* pointer to listen structure */
int ret;
int port = 7238;		/* port number for listening */
CLIENT_FD_T client[MAX_CLIENTS];
int	idx;			/* index at client list */
int	size;			/* actual buffer length */

    /* so_debug = 1; */

    /* open socket */
    if ((listen_fd = so_open()) == NULL)  {
	fprintf(stderr, "open socket failed\n");
	exit(1);
    }

    /* prepare server */
    if ((ret = so_server(listen_fd, port, &client[0], MAX_CLIENTS))
		!= 0)  {
	fprintf(stderr, "server failed, errno:  %d\n", ret);
	exit(1);
    }

    /* allset is a file descriptor set storing the fd's we're interested in */
    /* tcp sets are done by so_server() - now set own fd's */
    /* FD_SET(can_fd, &listen_fd->allset); */

    /* wait for connections or signals */
    while (1)  {
	/*                   fd         index   buffer     read chars*/
	size = MAXLINE;
	ret = so_server_doit(listen_fd, &idx, &buffer[0], &size);

	switch (ret)  {
	    case SRET_SELECT_ERROR:
		printf("select returns value < 0\n");
		break;
	    case SRET_CONN_FAIL:
		printf("new client connection wasn't possible\n");
		break;
	    case SRET_UNKNOWN_REASON:
		printf("unknown reason for select interrupt\n");
		break;
	    case SRET_CONN_CLOSED:
		printf("client at idx: %d closed connection\n", idx);
		break;
	    case SRET_CONN_NEW:
		printf("new client fd: %d at idx: %d\n", client[idx], idx);
		break;
	    case SRET_CONN_DATA:
		printf("message from fd: %d: idx %d (%d chars): %s\n",
		    client[idx], idx, size, &buffer[0]);
		break;
	    case SRET_SELECT_USER:
		printf("handle from user\n");
		break;
	    default:
		printf("unknown return value from so_server_doit\n", ret);
	}
    }


    /* close socket */
    so_close(listen_fd);
}

