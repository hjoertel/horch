
// *************************************************************************//
// FTEST                                                                    //
// Programm zum Testen einer Prozedur f�r die Zerlegung eines Parameter-    //
// strings -f.... (int read_fp_string(char *fp_string)) und zum Testen einer//
// Filter Prozedur(int filter(unsigned int id)).                            //
// Das Programm kann mit folgenden Parameterstrings aufgerufen werden       //
//  -f....            zu testender Filterparameterstring                    //
//  -timeAAAA         Die Filterprozedur wird mit einem Signalgenrator      //
//                    (random()) getestet. Mit AAAA kann eine Verz�gerungs- //
//                    zeit                                                  //
//                             5 ms <= Tv <= 9999 ms                        //
//                    eingestellt werden. Die Zeit Tv wird abgewartet bis   //
//                    vom Signalgenerator eine neue Nachricht f�r den       //
//                    Filtertest generiert wird                             //
//   -testABC         Mit den Ziffern A,B,C kann die Ausgabe von Test-      //
//                    informationen gesteuert werde:                        //
//                     A: Steuerparameter f�r Anzeigen zur Pr�fung des      //
//                        Filteralgorithmus                                 //
//                        0:  keine Anzeige                                 //
//                        >0: Anzeige aller Vergleiche                      //
//                    B: Steuerparameter f�r Anzeigen zur Pr�fung des       //
//                       Sortierens des Filterarrays und der Zusammenfassung//
//                       �berlappender Filterbereiche                       //
//                       0:  keine Anzeige                                  //
//                       1:  Anzeige aller Schritte zur Zusammenfassung von //
//                           Filterbereichen                                //
//                       >1: wie 1 + Anzeige des Filterarrays vor und       //
//                           nach dem Sortieren                             //
//                    C: Steuerparameter f�r Anzeigen zur Pr�fung der       //
//  							 Zerlegung des Parameterstrings             //
//								 0:  keine Anzeige                          //
//								 1:  Anzeige der ermittelten Bereiche       //
//								 >1: Anzeige aller verwendeten Teilsstrings //
//                                                                          //
// WERNER BR�UTIGAM                                                         //
// SCHULSTR. 11                                                             //
// 06343 VATTERODE                                                          //
// FAX/Tel:  034782-20716                                                   //
// Tel.:     034782-20555                                                   //
// E-Mail:   Werner.Braeutigam@t-online.de                                  //
// *************************************************************************//

#include <stdio.h>
/* #include <dos.h> */
#include <stdlib.h>
/* #include <conio.h> */
#include <string.h>

#include "filter.h"

#define getch readch

#define TRUE       1
#define FALSE      0

int debug;

void delf_char_cpy(char *sc, char del_char);           // Stringroutine
int is_number(const char *field);                      // Test auf Ziffern
int is_hex(const char *field);                         // Test auf Hexadezimalzahl
							// in der Form 0x3fd
int keyhit(void);                                 // Warten auf Tastenanschlag


//**************************************************************************//
// Hauptprogram:  main                                                      //
// Parameter...:  argc           Anzahl der �bergeben Parameterstrings      //
//                ** argv        Pointer auf ein Stringarray, da� die       //
//				 Parameterstrings enth�lt                   //
// Beschreibung:  Das Hauptprogramm dient als Testumgebung f�r die          //
//                Prozeduren read_fp_string(char *fp_string) und            //
//                Dazu werden folgende Schritte ausgef�hrt:                 //
//                 - Ausgabe der Parameterstrings                           //
//                 - Pr�fung der Parameterstrings                           //
//                 - Auswertung der Parameterstrings                        //
//                   -f..., -test... und -ftime...                          //
//                 - Ausgabe der aus den Parameterstrings ermittelten       //
//                   Programmparameter                                      //
//                 - Ausgabe der im Filterarray gespeicherten Bereiche      //
//                 - Test des Filters mit automatisch genererierten         //
//                   Messages                                               //
//                 - Ausgabe von Testinformationen in Abh�ngigkeit          //
//                   von -test...                                           //
//                 - Steuerung der Signalrate in Abh�ngigkeit von -Time...  //
//**************************************************************************//

int main(int argc, char ** argv)
{
 int i;                     // Laufvariable
 int found;                 // TRUE wenn Wert gefunden
 int finish;                // TRUE Filtertest beenden
 int f_str;                 // Nummer des Parameterstrings -f...
 int test_str;              // Nummer des Parameterstrings -test...
 int time_str;              // Nummer des Parameterstrings -time...
 int test_mod;              // Wert des Parameterstrings -test...
 unsigned int m_id;         // Vom Signalgerator [random()] erzeugte
			    // Message ID
 unsigned int delay_time;   // Verz�erungszeit in ms zur Steuerung der vom
			    // Signalgerator erzeugten Message Rate
 char string[16];           // Zeichenkette zur Speicherung von Zwischenwerten

 printf("\n\n\rProgramm Filtertest\n\r");
 printf("===================\n\n\r");
 // Anzeige Kommandozeilenparameter
 printf("\n\rKomandozeilenparameter  Inhalt\n\r");
 i = 0;
 while (i < argc )
  {
	printf("%5d                   %s\n\r",i,argv[i]);
	i++;
  } // while (i < argc )

// Kommandozeilenparameter Filter "-f..."  suchen
 printf("\n\n\r");
 f_str = -1;
 test_str = -1;
 time_str = -1;

 //Bestimmung auswertbarer Parameterstrings
 i = 1;
 while (( i < argc))
  {
	found = FALSE;
	if ((argv[i][0] == '-') && (argv[i][1] == 'f')&& (f_str == -1))
	  {
		f_str = i;
		found = TRUE;
	  } 
	if ((argv[i][0] == '-') && (argv[i][1] == 't')&& (argv[i][2] == 'e')&&
		 (argv[i][3] == 's')&& (argv[i][4] == 't')&& (test_str == -1))
	  {
		test_str = i;
		found = TRUE;
	  }
	if ((argv[i][0] == '-') && (argv[i][1] == 't')&& (argv[i][2] == 'i')&&
		 (argv[i][3] == 'm')&& (argv[i][4] == 'e')&& (time_str == -1))
	  {
		time_str = i;
		found = TRUE;
	  }
	if (found == FALSE)
	  {
		printf("Parmeterliste ungueltig!\n\r");
		printf("Zulaessig sind -f..., -test... und -time...!\n\r");
		printf("Bitte korrigieren Sie die Parameterliste!\n\r");
		printf("\n\r************** Programm beendet! ***************\n\r");
		exit(-1);
	  } // if (found == FALSE)
	i++;
  } // while (( i < argc))
 kbdinit();
 keyhit();

 // Bestimmung und Ausgabe der Programmparameter
 printf("\n\rParameter:\n\r");
 printf("==========\n\r");

 // Festlegung der Verz�gerung bis zur n�chsten Erzeugung einer Message ID
 // Auswertung Parameterstring  "-time..."
 if (time_str < 0)
	{
	 delay_time = 1000;
	} // (time_str < 0)
  else
	{
	 if (strlen(argv[time_str])  > 9)
		{
		 printf("\n\rPrameter %s zu lang(maximal 9 Zeichen)!", argv[time_str]);
		 printf("\n\r************** Programm beendet! ***************\n\r");
		 exit(-1);
		} // if (strlen(argv[time_str])  > 9)
	  else
		{
		 a_char_cpy(string, argv[time_str], 'e');
		 if (is_number(string) == TRUE)
			{
			 delay_time = (unsigned int)strtoul(string, NULL, 10);
			} // if (is_number(string) == TRUE)
		  else
			{
			 printf("\n\rPrameter %s ist ung�ltig!", argv[time_str]);
			 printf("\n\r************** Programm beendet! ***************\n\r");
			 exit(-1);
			} // else --> if (is_number(string) == TRUE)
	  } // else --> if (strlen(argv[time_str])  > 9)
	} // else --> (time_str < 0)
  if (delay_time < 5) delay_time = 5;
  printf("\n\rVerzoegerungszeit: %5d ms\n\r",delay_time);

 // Festlegung des Testmodus
 // Auswertung des Parameterstrings "-test...
 if (test_str < 0)
	{
	 test_mod = 0;
	} // if (test_str < 0)
  else
	{
	 if (strlen(argv[test_str])  > 8)
		{
		 printf("\n\rPrameter %s zu lang(maximal 8 Zeichen)!", argv[test_str]);
		 printf("\n\r************** Programm beendet! ***************\n\r");
		 exit(-1);
		} // if (strlen(argv[test_str])  > 8)
	  else
		{
		 a_char_cpy(string, argv[test_str], 's');
		 delf_char_cpy(string, 't');
		 if (is_number(string) == TRUE)
			{
			 test_mod = atoi(string);
			 if (test_mod >99)
				{

				 test_mod_para_str = (test_mod % 100) % 10;
				 test_mod_range_ar = (test_mod % 100) / 10;
				 test_mod_filter =    test_mod / 100;
				} // if (test_mod >99)
			  else
				{
				 if (test_mod >9)
					{
					 test_mod_para_str =  test_mod % 10;
					 test_mod_range_ar =  test_mod / 10;
					 test_mod_filter =    0;
					} // if (test_mod >9)
				  else
					{
					 test_mod_para_str =  test_mod;
					 test_mod_range_ar =  0;
					 test_mod_filter =    0;
					} // else --> if (test_mod >9)
				}  // if (test_mod >99)
			} // if (is_number(string) == TRUE)
		  else
			{
			 printf("\n\rPrameter %s ist ung�ltig!", argv[time_str]);
			 printf("\n\r************** Programm beendet! ***************\n\r");
			 exit(-1);
			} // --> if (is_number(string) == TRUE)
		}  // else --> if (strlen(argv[test_str])  > 8)
	} // else --> if (test_str < 0)
 printf("Testmodus:         %5d\n\r",test_mod);

 // Bestimmung Filterbereiche
 // Auswertung Filterstring "-f...."
 if (f_str < 0)
	{
	 // Kein Filterparameter im Parameterstring
	 printf("Filterparameter:   keine\n\r");
#if 0
	 r_array[0].min = RANGE_MIN;
	 r_array[0].max = RANGE_MAX;
	 n_range = 0;
#endif
	} // if (f_str < 0)
  else
	{
	 printf("Filterparameter:   %s\n\r", argv[f_str]);
	 if (read_fp_string(argv[f_str])== FALSE)
		{
		 printf("\n\rBitte korrigieren Sie den Filterparameter f-...!\n\r");
		 printf("\n\r************** Programm beendet! ***************\n\r");
		 exit(-1);
		} // if (read_fp_string(argv[f_str])== FALSE)
	} // else --> f (f_str < 0)

// Ausgabe Filterbereiche
    f_array_h();
#if 0
 i = -1;
 printf("\n\rFilterbereiche:\n\r");
 printf("Nummer   Minimum     Maximum\n\r");
 while (i < n_range)
  {
	i++;
	printf("%5d    %5u       %5u  dezimal\n\r", i,r_array[i].min,r_array[i].max );
	printf("%5d    %5X       %5X  hexadezimal\n\n\r", i,r_array[i].min,r_array[i].max );
  } // while (i < n_range)
#endif
 keyhit();

 // Hauptschleife f�r Filtertest
 printf("\n\rFiltertest:\n\r");
 printf("===========\n\n\r");
 printf("Filtertest beenden durch einen beliebigen Tastendruck!\n\n\n\n\n\r");
 keyhit();

 /* randomize(); */
 i = 0;
 finish = FALSE;
 while (finish == FALSE)
  {
	// Message-Genarator
	/* m_id = random (RANGE_MAX); */
	m_id = random () % 1000;
	i++;

	// Ausgabe Kopf Ergenistabelle
	if ((i % 20) == 1)
	  {
		printf("\n\rMessage  Filterinput     Filteroutput");
		printf("\n\r          HEX   Dezimal   HEX   Dezimal\n\r");
	  } // if ((i % 20) == 1)

	// Filter und Ausgabe Filterinput und Filteroutput
	if (filter(m_id) == TRUE)
	  {
		printf("%5u   %5X     %5u %5X     %5u\n\r",i,m_id, m_id,m_id, m_id);
	  } // if (filter(m_id) == TRUE)
	 else
	  {
		printf("%5u   %5X     %5u\n\r",i,m_id,m_id);
	  } // else --> if (filter(m_id) == TRUE)
	 /* delay(delay_time); */
	 usleep(delay_time * 1000);
	 if (kbhit () != 0) finish = TRUE;
  } // while (finish == FALSE)
 printf("\n\r************** Programm beendet! ***************\n\r");
 exit(0);

 kbdexit();
} // void main(int argc, char ** argv)





//**************************************************************************//
// Prozedur....:  is_number                                                 //
// Parameter...:  *field       Zeichenkette (Quelle)                        //
// Beschreibung:  Die Prozedur pr�ft, ob alle Zeichen der Zeichenkette      //
//                Dezimalziffern '0' - '9' sind                                 //
// R�ckgabewert:  TRUE: Alle Zeichen von field sind Dezimalziffern          //
//                FALSE: Mindestens ein Zeichen von field ist keine         //
//                       Dezimalziffer                                      //
//**************************************************************************//

int is_number(const char *field)
{
 int index_field;
 char s;
 index_field = 0;
 s = field[index_field];
 while (s != 0 )
	{
	if ((s < '0') || (s > '9')) return(FALSE);
	index_field++;
	s = field[index_field];
	}  // while (s != 0 )
 return(TRUE);

}  // int is_number(const char *field)

//**************************************************************************//
// Prozedur....:  is_hex                                                    //
// Parameter...:  *field       Zeichenkette (Quelle)                        //
// Beschreibung:  Die Prozedur pr�ft, ob die Zeichenkette eine Hexadezimal- //
//                zahl der Form                                             //
//                               0xZ[Z]                                     //
//                enth�lt. Die Ziffern Z k�nnen die Werte '0' - '9',        //
//                'A' - 'F' oder 'a' - 'f'  annehmen.                       //
// R�ckgabewert:  TRUE:  Die ZeichenKette enth�lt eine Hexadezimalzahl      //
//                       der angegeben Form.                                //
//                FALSE: Die Zeichenkette enth�lt keine Hexadezimalzahl     //
//                       der angegeben Form.                                //
//**************************************************************************//

int is_hex(const char *field)
{
 int index_field;
 char s;
 if (strlen(field) < 3) return(FALSE);
 if ((field[0] != '0') || (field[1] != 'x'))return(FALSE);
 index_field = 2;
 s = field[index_field];
 while (s != 0 )
	{
	 if (((s >= '0') && (s <= '9')) || ((s >= 'a') && (s <= 'f')) ||
		  ((s >= 'A') && (s <= 'F')))
		{
		 index_field++;
		 s = field[index_field];
		}  // if (((s >= '0') && (s <= '9')) || ((...
	  else
		{
		 return(FALSE);
		} // else --> if (((s >= '0') && (s <= '9')) || ((...
	} // while (s != 0 )
 return(TRUE);

}  // int is_hex(const char *field)

//**************************************************************************//
// Prozedur....:  keyhit                                                    //
// Parameter...:                                                            //
// Beschreibung:  Die Prozedur wartet auf einen Tastenanschlag einer        //
//                beliebigen Taste und gibt einen entsprechenden Hinweis    //
//                aus.                                                      //
//**************************************************************************//

int keyhit(void)
{
 printf("\nFortsetzung mit beliebiger Taste ...");
 while (kbhit()!= 0) getch();
 while (kbhit () == 0);	// wartet bid Tastenanschlag
 getch();
 printf ("\n");
 return(TRUE);

} // int keyhit(void)

//**************************************************************************//
// Prozedur....:  delf_char_cpy                                             //
// Parameter...:  char *sc       Zeichenkette (Quelle)                      //
//						char seek_char Zeichen                                    //
// Beschreibung:  Die Prozedur entfernt alle Zeichen seek_char am Anfang    //
//                der Zeichenkette sc. Ab dem ersten von seek_char          //
//                verschiedenen Zeichen werden alle Zeichen nach sc kopiert.//
//**************************************************************************//

void delf_char_cpy(char *sc, char del_char)
{
 int Indexsc, Indexres, copy;
 Indexsc = 0;
 Indexres = 0;
 copy = FALSE;
 if  (sc[0] != del_char) return;
 while (sc[Indexsc] != 0)
	{
	if (sc[Indexsc] != del_char) copy = TRUE;
	if (copy)
		{
		sc[Indexres] =  sc[Indexsc];
		Indexres++;
		} // if (copy)
	Indexsc++;
	} //  while (sc[Indexsc] != 0)
 sc[Indexres] = 0;

} // void delf_char_cpy(char *sc, char del_char)

