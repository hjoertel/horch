
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "filter.h"


#define TRUE       1
#define FALSE      0

int debug;


int main(int argc, char ** argv)
{
 int i;                     // Laufvariable
 int found;                 // TRUE wenn Wert gefunden
 char filterdesc[200];
 unsigned int m_id[100] =
    {
    1 , 2, 9, 10, 11, 12, 20, 40, 59, 60, 61, 62, 90,
    99, 100, 101, 200, 300, 400, 598, 599, 600, 601, 602,
    800, 900, 999, 1000, 1001, 1002, 1200, 1400, 1500, 1598, 1599,
    1600, 1601, 1602, 2000, 3000, -1 };

    printf("Programm Filtertest\n");
    printf("===================\n");


    		/* 10-60,100-600,1000-1600 */
#define FILTER "10-60,100-600,0x3e8-0x640"
    printf("Filter: %s\n\n", FILTER);
    strcpy(filterdesc, FILTER);
    read_fp_string(filterdesc);

    i = 0;
    while( m_id[i] != -1) {
	found = filter(m_id[i]);
	printf("%8d %s\n", m_id[i], found == TRUE ? "Ja" : "Nee");
	i++;
    }
}




