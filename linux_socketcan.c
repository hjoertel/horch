/*
 * linux - SocketCAN device specific part of Horch
 *
 *
 * Copyright (C) 2015 H.-J. Oertel, <oe@emtas.de>
 *------------------------------------------------------------------
 * $Id: linux.c 19 2015-07-15 13:41:19Z hjoertel $
 *
 *--------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *
 *
 *
 *
 * This Sourcefile contains:
 *
 * - SocketCAN specific setup (only one phys device can0)
 * - Server mode Loop 
 * - Console Loop
 * - direct access to the SocketCAN device
 *
 */

/* OS headers
---------------------------------------------------------------------------*/
#include <net/if.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#include "horch_cfg.h"

#include "socklib/socklib.h"

#include <horch.h>
#ifdef CONFIG_COLDFIRE 
# define CONFIG_LED_SUPPORT
#endif

#ifdef CONFIG_LED_SUPPORT
# include "led.h"
#endif

#ifdef USER_TERMIOS

# define  HAVE_TERMIOS
# include <unistd.h>
# include <fcntl.h>
# include <sys/ioctl.h>
# include <sys/time.h>
# include <termios.h>
# include <sys/resource.h>

#endif

/* constant definitions
---------------------------------------------------------------------------*/

/* May be an old socketcan version is  used, not having already CANFD support */
#if !defined CANFD_MAX_DLEN
//# define CAN_MAX_DLC 8
# define CAN_MAX_DLEN    8
# define CANFD_MAX_DLEN 64
# define CAN_MTU	(sizeof(struct can_frame))
# define CANFD_MTU	72 /* (sizeof(struct canfd_frame)) */
#endif


#ifndef CAN_INTERFACE
#  define CAN_INTERFACE "can0"
#endif /* CAN_INTERFACE */

#define MAXLINE		1024
#define MSG_TIMEOUT	100
#define RET_DRV_ERROR	-1


/* local defined variables
---------------------------------------------------------------------------*/
static int    mBSDSocket;	/*!< handle of BSD socket */
static struct timeval	tv, tv_start;
static struct timezone	tz;
const char * interface_cstr = CAN_INTERFACE;

struct sockaddr_in fsin;		/* UDP socket */
struct sockaddr_can addr;

/* If recvmsg is used */
char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
static __u32 dropcnt[HORCH_MAX_CLIENTS];
static __u32 last_dropcnt[HORCH_MAX_CLIENTS];


#ifdef HAVE_TERMIOS
static struct termios oldtty;
#endif

static void clean(void);
/*
 * =========================================================
 * LINUX system specific part
 * =========================================================
 */

/**************************************************************************
*
* set_up - Linux / SocketCAN specific initialisation
*
* - CAN Interface
* - console
*
*/
int set_up(void)
{
int ret;
char line[40];
struct ifreq ifr;
#if defined CANFD
const int canfd_on = 1;
int mtu;
#endif


    atexit(clean);

    /* create RAW socket for CAN */
    mBSDSocket = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (mBSDSocket < 0) {
        perror("socket");
        return RET_DRV_ERROR;
    }

    /* link socket to specified CAN interface */
    addr.can_family = AF_CAN;
    strcpy(ifr.ifr_name, interface_cstr);
#ifdef DEBUG
    printf("using SocketCAN device %s\n", ifr.ifr_name);
#endif
    if (ioctl(mBSDSocket, SIOCGIFINDEX, &ifr) < 0) {
        perror("SIOCGIFINDEX");
        return  RET_DRV_ERROR;
    }
    addr.can_ifindex = ifr.ifr_ifindex;

#if defined CANFD
    /* check if the frame fits into the CAN netdevice */
    if (ioctl(mBSDSocket, SIOCGIFMTU, &ifr) < 0) {
	    perror("SIOCGIFMTU");
	    return RET_DRV_ERROR;
    }
    mtu = ifr.ifr_mtu;

    if (mtu != CANFD_MTU) {
	    fprintf(stderr, "CAN interface is not CAN FD capable - sorry.\n");
	    return RET_DRV_ERROR;
	}
#endif


    /* bind to socket, so that we can receive messages from it */
    if (bind(mBSDSocket, (const struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("bind");
        return RET_DRV_ERROR;
    }

#if defined CANFD
    /* try to switch the socket into CAN FD mode */
    if (setsockopt(mBSDSocket, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
			&canfd_on, sizeof(canfd_on))) {
		fprintf(stderr, "error when enabling CAN FD support\n");
    }
#endif


    do {
    int rcvbuf_size = 500000; 
	    if (rcvbuf_size) {

		    int curr_rcvbuf_size;
		    socklen_t curr_rcvbuf_size_len = sizeof(curr_rcvbuf_size);

		    /* try SO_RCVBUFFORCE first, if we run with CAP_NET_ADMIN */
		    if (setsockopt(mBSDSocket, SOL_SOCKET, SO_RCVBUFFORCE,
				   &rcvbuf_size, sizeof(rcvbuf_size)) < 0) {
#ifdef DEBUG
			    printf("SO_RCVBUFFORCE failed so try SO_RCVBUF ...\n");
#endif
			    if (setsockopt(mBSDSocket, SOL_SOCKET, SO_RCVBUF,
					   &rcvbuf_size, sizeof(rcvbuf_size)) < 0) {
				    perror("setsockopt SO_RCVBUF");
				    break;
				    //return 1;
			    }

			    if (getsockopt(mBSDSocket, SOL_SOCKET, SO_RCVBUF,
					   &curr_rcvbuf_size, &curr_rcvbuf_size_len) < 0) {
				    perror("getsockopt SO_RCVBUF");
				    break;
				    //return 1;
			    }

			    /* Only print a warning the first time we detect the adjustment */
			    /* n.b.: The wanted size is doubled in Linux in net/sore/sock.c */
			    if (/*!i &&*/ curr_rcvbuf_size < rcvbuf_size*2)
				    fprintf(stderr, "The socket receive buffer size was "
					    "adjusted due to /proc/sys/net/core/rmem_max.\n");
		    }
	    }
    } while(0);

    /* read back */
    {
    int rcvbuf_size = 0;
    unsigned int cnt = sizeof(rcvbuf_size);
	    if (getsockopt(mBSDSocket, SOL_SOCKET, SO_RCVBUF,
			       &rcvbuf_size, &cnt) >= 0) 
	    {
		    //printf("rxbuf %d (%d==%d)\n", rcvbuf_size, cnt, sizeof(rcvbuf_size));
		    printf("rxbuf %d\n", rcvbuf_size);
	    }
    }

/*----- enable error frames -------------------------------------------*/
    {
    can_err_mask_t err_mask = CAN_ERR_FLAG | CAN_ERR_CRTL | CAN_ERR_PROT | CAN_ERR_BUSOFF;//0xFFFFFFFFu; //all error

	    if (setsockopt(mBSDSocket, SOL_CAN_RAW, CAN_RAW_ERR_FILTER,
				       &err_mask, sizeof(err_mask)) < 0)
	    {
		    fprintf(stderr,"no error messages possible\n");
	    }
    }
/*----- enable time stamp ---------------------------------------------*/
    {
    const int timestamp_on = 1;

	if (setsockopt(mBSDSocket, SOL_SOCKET, SO_TIMESTAMP,
		       &timestamp_on, sizeof(timestamp_on)) < 0) {
		perror("setsockopt SO_TIMESTAMP");
		return 1;
	}
    }

/*---------------------------------------------------------------------*/



#if defined(CAN4LINUX)
    BDEBUG("message structure canmsg_t has %ld bytes\n", sizeof(canmsg_t));
#endif

    if(!o_server) {
	/* set terminal mode */
#ifdef USER_TERMIOS
	struct termios tty;

	if(debug) {
	    printf("Change terminal settings using tcsetattr(3)\n");
	}

	tcgetattr (0, &tty);
	oldtty = tty;

	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP
			      |INLCR|IGNCR|ICRNL|IXON);
	tty.c_oflag |= OPOST;
	tty.c_lflag &= ~(ECHO|ECHONL|ICANON|IEXTEN);
	tty.c_cflag &= ~(CSIZE|PARENB);
	tty.c_cflag |= CS8;
	tty.c_cc[VMIN] = 1;
	tty.c_cc[VTIME] = 0;

	tcsetattr (0, TCSANOW, &tty);
	signal(SIGQUIT, clean_up); /* Quit (POSIX).  */
#else
	if(debug) {
	    printf("Change terminal settings using stty(1)\n");
	}
	ret = system("stty cbreak -echo");
	if(ret != 0) {
	    fprintf(stderr, "  system(stty) returns %d\n", ret);
	    fflush(stderr);
	}
#endif
    }

    /* pe-set time structures */
    gettimeofday(&tv_start, &tz);
    return 0;
}

/**************************************************************************
*
* clean_up
*
*/
void clean_up(int sig)
{
    (void)sig;		/* not evaluated */

    if(debug > 1) {
	printf("Got Signal %d. Clean process and terminal settings.\n",
		sig);
    }
#ifndef SIM
    close(mBSDSocket);
#endif

#ifdef USER_TERMIOS
    tcsetattr (0, TCSANOW, &oldtty);
#endif
    /* clean(); */ /* clean wird per atexit() eingebunden */
    exit(0);
}

/**************************************************************************
*
* udp_event_loop
*
*/
int udp_event_loop(void)
{
    fprintf(stderr, "UDP communication not yet implemented.\n");    
    return -1;
}

/**************************************************************************
*
* server_event_loop
*
*/
int server_event_loop(void)
{
/*----------------------------------------------------------------*/
int i;				/* looping index */
char in_line[MAXLINE];		/* command input line from socket to horch */
int ret;
int size;	/* buffer size and filled buffer count */
int idx;	/* index at client list */
unsigned char client; /* loop var */

SOCKET_T * pSocket;


/* here the socket debug message display can be activated
 * it will influence the socklib functions */
/* extern int so_debug; */
    /* so_debug = 1; */

    /*
     * Open a TCP socket (an Internet stream socket).
     * 
     */
    pSocket = so_open();
    if( pSocket == NULL ) {
	/* fail management cuold be better */
	fprintf(stderr, "Socket open failed: %d\n", errno);

	return 0;
    }

    /* prepare server */
    ret = so_server( pSocket, o_portnumber, &client_fd[0], HORCH_MAX_CLIENTS); 
    if( ret != 0 ) {
	fprintf(stderr, "server failed: %d\n", ret);
	/* so_server() schlie�t bereits den Socket */
	/* so_close(pSocket); */
	return 0;
    }

    

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
{
canmsg_t rx;			/* buffer for can4linux reference struct */
struct can_frame frame;		/* SocketCAN frame struct for read() call */
int got;			/* got this number of messages */


  //  while( read(mBSDSocket, &frame , sizeof(frame)) > 0); /* flush/remove old messages */

    FD_SET(mBSDSocket, &(pSocket->allset));	/* watch on fd for CAN */

    /* Initialisierung ready */
    BDEBUG("Waiting for Connections on Port %d\n", o_portnumber);

    /*
    * loop forever (daemons never die!)
    *
    * the loop is waiting for new client connections or disconnections
    * or new CAN messages arriving 
    */
    for ( ; ; ) {

        size = MAXLINE;
#ifdef __uClinux__
	ret = so_server_doit(pSocket, &idx, &in_line[0], &size, 2);
#else
	ret = so_server_doit(pSocket, &idx, &in_line[0], &size, 0);
#endif

	if(debug) {
	    switch (ret)  {
		case SRET_SELECT_ERROR:
		    /* z.B. Timer (f�r Buslastmessung) */
		    /* BDEBUG("select returns value < 0\n"); */
		    break;
		case SRET_CONN_FAIL:
		    printf("new client connection wasn't possible\n");
		    break;
		case SRET_UNKNOWN_REASON:
		    BDEBUG("unknown reason for select interrupt\n");
		    break;
		case SRET_CONN_CLOSED:
		    printf("client at idx: %d closed connection\n", idx);
		    break;
		case SRET_CONN_NEW:
		    printf("new client idx: %d\n", idx);
		    break;
		case SRET_CONN_DATA:
		    if(debug > 1) {
			printf("message from fd: %d: idx %d (%d chars): %s\n",
			    client_fd[idx], idx, size, &in_line[0]);
		    }
		    break;
		case SRET_SELECT_USER:
		    if(debug > 2) {
			printf("handle from user\n");
		    }
		    break;

		default:
		    printf("unknown return value from so_server_doit\n", ret);
	    }
	}

	/*------------------------------------------------------*/
	if( ret == SRET_CONN_NEW) {
		/* new Client - initialize */
	    filter_init(idx);		/* filter */
	    reset_send_line(idx, -1);	/* empty transmit buffer of this client */
	    /* Set client into special modes:
	     * https://tools.ietf.org/html/rfc854
	     *  - Telnet Protocol Specification
	     * https://tools.ietf.org/html/rfc1116 
	     * 	- Telnet Linemode Option
	     * IAC WONT LINEMODE
	     */
       printf("send IAC code to fd %d\n", client_fd[idx]);
	    write(client_fd[idx], "\377\375\042",3);
	    /* IAC WONT ECHO */
	    write(client_fd[idx], "\377\375\001",3);


#ifdef CONFIG_LED_SUPPORT
	/* Connect LED On */
	    if (led_open() == 0) {
		led_set(LED_STATUS1, LED_ON);
		led_close();
	    }
#endif /* CONFIG_LED_SUPPORT */

	}
	/*------------------------------------------------------*/

	/*------------------------------------------------------*/
	if( ret == SRET_CONN_CLOSED) {
	int f = 0;	/* Flag */
		/* check for other open connections */
	    for(client = 0; client < HORCH_MAX_CLIENTS; client++) {
		if (client_fd[client] != NO_CLIENT) {
		    f = 1;
		    break;
		}
	    }
	    if (f == 0) {
	    	/* Stop_CAN(); */
#ifdef CONFIG_LED_SUPPORT
		if (led_open() == 0) {
		    led_set(LED_STATUS1, LED_OFF);
		    led_close();
		}
#endif /* CONFIG_LED_SUPPORT */
	    }
	}
	/*------------------------------------------------------*/
	
	/*------------------------------------------------------*/
	if(ret == SRET_SELECT_USER) {
	    if (FD_ISSET(mBSDSocket, &pSocket->allset))
	    {
		/* it was the CAN fd, read only one frame at the time */
		got=read(mBSDSocket, &frame , sizeof(frame));

		if( got > 0) {

		    /* got Messages from the read() call */
		    if (debug > 2) {
			/* fprintf(stderr, "--------------\n"); */
			fprintf(stderr, "Received got = %d\n", got);
		    } 
		   
		    /* clean message buffer before evaluating frame */
		    memset(&rx, '\0', sizeof(canmsg_t));

		    /* check for CAN error information */
		    if (frame.can_id & CAN_ERR_FLAG) {
			/* FIXME: not handled yet */
		    } else {
			int j;
			if (debug > 2) {
			    printf("Convert SocketCAN frame to can4linux frame\n");
			}

			/* copy CAN frame */
			rx.id = frame.can_id & 0x1FFFFFFFul;
			if (frame.can_id & CAN_RTR_FLAG) {
				rx.flags |= MSG_RTR; 
			} 

			if (frame.can_id & CAN_EFF_FLAG) {
				rx.flags |= MSG_EXT; 
			}

			rx.length = frame.can_dlc;

		    
			for (j = 0; j < rx.length; j++) {
				rx.data[j] = frame.data[j];
			}
			show_message(&rx);
		    }


		    /* send buffer to the clients */
		    for(client = 0; client < HORCH_MAX_CLIENTS; client++) {
			if (client_fd[client] == -1) {
			    continue;
			}

			/* formatted string reaches Buffer end !*/
    /* fprintf(stderr, "=> send line\n"); */
			display_line(client);
			
			/* send cyclic statistic if possible */
			sendStatisticInformation(client);
		    } /* for all clients */
		} else {
		    /* read returned with error */
		    /* fprintf(stderr, "- Received got = %d\n", got); */
		    /* fflush(stderr); */
		}

	    } /* it was the CAN fd */
	    else {
	    	/* timeout or time event */
		for(client = 0; client < HORCH_MAX_CLIENTS; client++) {
		    if (client_fd[client] == NO_CLIENT) {
			continue;
		    }
		    /* send statistic if possible */
		    sendStatisticInformation(client);
		}
	    }
	}
	/*------------------------------------------------------*/

	/*------------------------------------------------------*/
        if( ret == SRET_CONN_DATA ) {
	    /* in Idx steht nun der client */

	    for(i = 0; i < size; i++) 
	    {
	    int retval;

		/* read input chars from recv buffer */
		retval = change_format(idx, in_line[i]);
		if(retval == -1) {
#ifdef DEBUGCODE		
		    /* ERROR */		    
		    fprintf(stderr,"change_format returns %d\n", retval);
		    fflush(stderr);
#endif
		    break;
		}
	    }
	} /* Server-in/stdio fd */
	/*------------------------------------------------------*/

    } /* for(; ; ;) */
} /* CAN definitions */
   /************************************/
   /* Shutdown server, should not happen */
   /************************************/

/* TCP_SERVER_DONE: */

    so_close(pSocket);
    return 0;

}

/**************************************************************************
*
* event_loop - Hauptschleife f�r die Arbeit innerhalb der Konsole
*
* Es wird (intern) immer mit client 0 gearbeitet.
*
*/
#define MAX_KEY_CHARS	40
#define CLIENT_STDOUT	0
void event_loop(void)
{
canmsg_t rx;			/* can4linux frame struct */
struct can_frame frame;		/* SocketCAN frame buffer for read */
    
fd_set rfds;
int got;				/* got this number of messages */
int i = 0;
struct timeval tval;			/* use time out in W32 server */
char keybuf[MAX_KEY_CHARS+1];		/* keys read with one read() call */


#if defined USED_READ
#else /* Use recvmsg() */
struct iovec iov;
struct msghdr msg;
struct cmsghdr *cmsg;
#endif

/* �NDERN bzw. es wird immer mit client 0 gearbeitet */
/* int client = 0; */

    /* Konsolen-Ausgabe benutzt Client 0 */	
    client_fd[CLIENT_STDOUT] = 1;

    filter_init(CLIENT_STDOUT);		/* filter */
    reset_send_line(CLIENT_STDOUT, -1); 	/* transmit buffer */
    /* Start_CAN();  */

    /* On LINUX we need no time out for the select call.
     * we either, wiat for:
     * a message arrives on mBSDSocket
     * a key was hit on stdin - fd=0
     */
    tval.tv_sec  = 0;			/* first try it with 1ms */
    tval.tv_usec = 1400;

    /* recvmsg: these settings are static and can be held out of the hot path */
    iov.iov_base = &frame;
    msg.msg_name = &addr;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = &ctrlmsg;

    while(1) {
        FD_ZERO(&rfds);
        FD_SET(mBSDSocket, &rfds);		/* watch on fd for CAN */
        FD_SET(0, &rfds);		/* watch on fd for stdin */

#if defined(TARGET_LINUX_PPC)
        /* select for:          read, write, except,  timeout */      
        if( select(FD_SETSIZE, &rfds, NULL, NULL,     &tval ) > 0 )
#else
        /* select for:          read, write, except, no-timeout */      
        if( select(FD_SETSIZE, &rfds, NULL, NULL,     NULL  ) > 0 )
#endif
        {
	int nbytes, maxdlen;

	    /* one of the read file descriptors has changed status */
	    /* fprintf(stderr, "."); fflush(stderr);         */
        
            if( FD_ISSET(mBSDSocket, &rfds) ) {
            	/* it was the CAN fd */
		/* LinuxCAN aka SocketCAN is unfortunately using a different 
		 * CAN frame structure */


#if defined USED_READ


		got=read(mBSDSocket, &frame, sizeof(frame));
		if( got > 0) {
		    /* Messages in read */
# ifdef DEBUGCODE 
		    if (debug) {
			/* fprintf(stderr, "--------------\n"); */
			fprintf(stderr, "Received got = %d\n", got);
		    } 
# endif 	/* DEBUGCODE */
#else
		/* these settings may be modified by recvmsg() */
		iov.iov_len = sizeof(frame);
		msg.msg_namelen = sizeof(addr);
		msg.msg_controllen = sizeof(ctrlmsg);  
		msg.msg_flags = 0;



		nbytes = recvmsg(mBSDSocket, &msg, 0);
		if (nbytes < 0) {
		    perror("read");
		    return;
		}
		if ((size_t)nbytes == CAN_MTU)
		    maxdlen = CAN_MAX_DLEN;
		else if ((size_t)nbytes == CANFD_MTU)
		    maxdlen = CANFD_MAX_DLEN;
		else {
		    fprintf(stderr, "read: incomplete CAN frame\n");
		    return;
		}


		if (1) {
		    BDEBUG("Hallo\n");


		    for (cmsg = CMSG_FIRSTHDR(&msg);
			 cmsg && (cmsg->cmsg_level == SOL_SOCKET);
			 cmsg = CMSG_NXTHDR(&msg,cmsg)) {
			    if (cmsg->cmsg_type == SO_TIMESTAMP)
				    tv = *(struct timeval *)CMSG_DATA(cmsg);
			    else if (cmsg->cmsg_type == SO_RXQ_OVFL)
				    dropcnt[0] = *(__u32 *)CMSG_DATA(cmsg);
		    }

#endif

		    /* clean message buffer before evaluating frame */
		    memset(&rx, '\0', sizeof(canmsg_t));
		    /* check for CAN error information */
		    if (frame.can_id & CAN_ERR_FLAG) {
			/* FIXME: not handled yet */
		    } else {
			int j;

			/* copy CAN frame */
			rx.id = frame.can_id & 0x1FFFFFFFul;
			if (frame.can_id & CAN_RTR_FLAG) {
				rx.flags |= MSG_RTR; 
			} 

			if (frame.can_id & CAN_EFF_FLAG) {
				rx.flags |= MSG_EXT; 
			}

			rx.length = frame.can_dlc;

		    
			for (j = 0; j < rx.length; j++) {
				rx.data[j] = frame.data[j];
			}

			/* copy time stamp */
			rx.timestamp.tv_sec  = tv.tv_sec;
			rx.timestamp.tv_usec = tv.tv_usec;


			if((rx.id < 0) || (filter(CLIENT_STDOUT, rx.id) == TRUE)) { 
				/* for all received messages */
				show_message(&rx);
			}
		    }
		} else {
		    /* read returned with error */
		    fprintf(stderr,
		    	"- Received got=%d\n", got);
		    fflush(stderr);
		}
	    } /* it was the CAN fd */

            if( FD_ISSET(0, &rfds) ) {
            	/* it was the stdio terminal fd */
            	i = read(0 , keybuf, MAX_KEY_CHARS);
            	while(i--) {
		    change_format(CLIENT_STDOUT, keybuf[i]);
		} /* while */
	    } /* stdio fd */
	} else {
	    /* timeout - or time event */
	    sendStatisticInformation(CLIENT_STDOUT);
	}
    }
}


/**************************************************************************
*
* clean
*
*/
static void clean(void)
{
    if(o_server) {
	;
    } else {
	if(debug) {
	    printf("Restore terminal settings using stty(1)\n");
	}
	(void)system("stty sane");
    }
}

/**************************************************************************
*
* show_system_time
*
*/
int show_system_time(char *line)
{
    gettimeofday(&tv, &tz);
    tv.tv_sec -= tv_start.tv_sec;
    /* tv.tv_usec /= 10000; */
    return(sprintf(line, "%12lu.%06lu  ", tv.tv_sec, tv.tv_usec));
    /* return(sprintf(line, "%3d.%02d  ", tv.tv_sec, tv.tv_usec)); */
}

/***********************************************************************
*
* write_message - write a can message with data from line
*
* .B Line
* contains information about a CAN message to be sent
* in ASCII format:
* .sp
* .CS
* [r] id 0{data}8
* .CE
* where r is a optional RTR Flag that has to be set.
* id is the CAN message identifier and data the optional zero to
* eight data bytes.
* the format of all numbers can be C-format decimal or hexa decimal number.
*
* RETURN:
*
*/



#define skip_space(p)  while(*(p) == ' ' || *(p) == '\t' ) (p)++
#define skip_word(p)  while(*(p) != ' ' && *(p) != '\t' ) (p)++

int write_message(
	int format,	/* if true - extended message format */ 
	char *line	/* write parameter line */
	)
{
unsigned char data[8] = {8, 7, 6, 5, 4, 3 , 2, 1};
unsigned char *lptr;
int len = 0;
/* unsigned char **endptr; */
unsigned char *endptr;
int rtr;			/* flag an RTR message */
#if defined CANFD
struct canfd_frame frame;
#else
struct can_frame frame;		/* transmit message struct */
#endif

    /* May be some check is needed if we have a valid and useful message */

    lptr = &line[0];
    skip_space(lptr);

    memset(&frame, '\0', sizeof(frame));
    rtr = FALSE;

    /* copy CAN data into Socket CAN format */

    if(*lptr == 'r' || *lptr == 'R') {
        rtr = TRUE;
	skip_word(lptr);
    }

    skip_space(lptr);
    frame.can_id  = strtoul( lptr, (char**)&endptr, 0);
    if(format == 1) {
        frame.can_id |= CAN_EFF_FLAG;
    }
    if(rtr) {
        frame.can_id |= CAN_RTR_FLAG;
    }


    while( lptr != endptr) {
        lptr = endptr;
        frame.data[len] = (signed char)strtol(lptr, (char**)&endptr, 0);
	if(lptr != endptr) len++;
	if (len == 8 ) break; 
    }

    frame.can_dlc = len;

    if(debug > 1) {
    	printf("Transmit %ld, RTR=%s, len=%d\n",
		frame.can_id,
		((rtr == 0) ? "F" : "T"),
		frame.can_dlc);
    }
			
    len = write(mBSDSocket, &frame, sizeof(frame));

    if (len < 0) {
    	/* Write Error */
	fprintf(stderr, "Write Error: %d\n", len);
    }
    
    if (len == 0) {
    	/* Transmit Timeout */
	fprintf(stderr, "Write Error: Transmit fehlgeschlagen\n");
    }

    return 0;
}	

/***********************************************************************
*
* set_acceptance - sets the CAN registers
*
* .B Line
* contains information about the content of the CAN
* registers "acceptance" and "mask"
* in ASCII format:
* .sp
* .CS
* 0x0707 0x00000000
* 1799
* .CE
* the format can be C-format decimal or hexa decimal number.
*
* Changing these registers is only possible in Reset mode.
*
* RETURN:
*
*/

int	set_acceptance(
	char *line
	)
{

}


/***********************************************************************
*
* set_bitrate - sets the CAN bitrate
*
* .B Line
* contains information about the new bit rate
* in ASCII format:
* .sp
* .CS
* 125
* 500
* 0x31c
* .CE
* the format can be C-format decimal or hexa decimal number.
*
* Changing these registers is only possible in Reset mode.
*
* RETURN:
*
*/

int	set_bitrate(
	char *line
	)
{

}


/***********************************************************************
* set_selfreception
*
* toggle the self reception ability of the CAN driver
*
* A message frame sent out by the controller is copied into
* the receive queue after successful transmission.
*/
void set_selfreception(int v)
{

}

/***********************************************************************
* set_timestamp
*
* toggle the time stamp ability of the CAN driver
*
* A received message is copied with a time information
* into the rx queue.
* This can take some �s. In order to shorten the CAN ISR.
* This can be switched off.
* In this case time stamp information is always zero.
*   0 - no time stamp (time stamp is zero)
*   1 - absolute time as gettimeofday()
*   2 - absolute rate monotonic time
*   3 - time difference to the last event (received message)
*

*/
void set_timestamp(int v)
{


}

/***********************************************************************
* getStat
*
* fill line with status info 
*
* Todo:
* We need to parameters. First the line for the standard information.
* Second a line for additional information. At the moment wie use a
* place holder for the standard information, that the horch add to the
* string. The standard information must have a fix position.
*
* Other solution, getState() fills a structure with all information. 
*
* Problem:
* At the moment the CAN-REport don't work with larger status information
* correctly. Therefore we add this information in a later version.
*/
void getStat(
	char *line
	)
{
# ifdef CONFIG_ADDITIONAL_STATUS_INFO
    /* default - not active! */
    sprintf(line, ":: %s %4d %2d %2d %2d %2d %2d %%s %d/%d %d/%d",
        m,
        status.baud,
        status.status,
        status.error_warning_limit,
        status.rx_errors,
        status.tx_errors,
        status.error_code,
        /* busload */
        status.tx_buffer_used,
        status.tx_buffer_size,
        status.rx_buffer_used
        status.rx_buffer_size,
        );
# else        /* CONFIG_ADDITIONAL_STATUS_INFO */
    sprintf(line, ":: %s %d %d %d %d %d %d",
        "SocketCAN",
        0 /* status.baud */,
        0 /* status.status */,
        0 /* status.error_warning_limit */,
        0 /* status.rx_errors */,
        0 /* status.tx_errors */,
        0 /* status.error_code */
        /* busload */
        );
# endif /* CONFIG_ADDITIONAL_STATUS_INFO */
}

/***********************************************************************
* getLayer2Version
* returns driver related part of version Information
*/
#define MAX_LAYERVERSION_STRING 200
const char * getLayer2Version(void)
{
static char s[MAX_LAYERVERSION_STRING];
FILE * fd;
char *ps;

    s[0] = 0;
    
    strncat(s, " can4linux Version: ", MAX_LAYERVERSION_STRING - strlen(s));

    fd = fopen("/proc/sys/Can/version","r");
    if (fd == NULL) {
	strncat(s, "???", MAX_LAYERVERSION_STRING - strlen(s));
    } else {
    	(void)fgets(s + strlen(s), 30, fd);
    	fclose(fd);
    }

    /* change control characters to spaces (e.g. linefeed) */
    ps = &s[0];
    while (*ps != 0) {
    	if (*ps < ' ') {
    	    *ps = ' ';
    	}
    	ps++;
    }
	    
    return s;		
}

