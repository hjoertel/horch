# Makefile for horch
#
# Copyright (C) 2005-12 H.-J. Oertel, port GmbH <oe@port.de>
# Copyright (C) 2013-2015 H.-J. Oertel, <oe@emtas.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
#
# A lot of targets may not work in the uClinux environment because
# they are ment to be used on other platforms like DOS or Windows PCs
#
# special Targets:
#    __W32__    .. ???
#    CYGWIN     .. ???
#    DOS16      .. ???
#    LX_WIN_GCC .. Level-X for Windows mit GCC (actual untested)
#    IPC        .. for old EtherCAN
#    LINUX_PPC  .. PPC / Linux
#			nimmt can4linux
#    LINUX      .. X86 / Linux
#			nimmt can4linux
#    LINUX_ARM   .. EtherCAN
#		    nimmt CPC Treiber
#    AC2I_WIN_BC .. AC2 ISA Card / Borland-C / Windows
#    AC2P_WIN_BC .. AC2 PCI Card / Borland-C / Windows
#    CPC_PP_WIN_BC .. W�nsche CPC-PP (old CPC-Interface)/ Borland-C / Windows
#    CPC_LINUX .. W�nsche CPC driver / Linux
#    CPC_ETHERCAN .. new EtherCAN / ARM (near CPC_ECO_LINUX)
#    CPC_ECO_WIN_BC .. W�nsche CPC ECO-PP and PCI (new CPC-Interface)
#			Borland-C / Windows
#    LINUX_COLDFIRE .. z.B. CTRLink, 5272 oder 5282 
#			nimmt can4linux
#    LINUX_BF       .. z.B. Analog Devices BF Stamp 537
#			nimmt can4linux
#    
TARGET=__W32__
TARGET=CYGWIN
TARGET=DOS16
TARGET=LX_WIN_GCC
TARGET=IPC
TARGET=AC2I_WIN_BC
TARGET=AC2P_WIN_BC
TARGET=CPC_PP_WIN_BC
TARGET=CPC_ETHERCAN
TARGET=LX_WIN_BC
TARGET=CPC_ECO_WIN_BC
TARGET=CPC_LINUX
TARGET=LINUX_PPC
TARGET=LINUX_ARM
TARGET=LINUX_BF
TARGET=LINUX_VCMA9
TARGET=LINUX_COLDFIRE
TARGET=LINUX

# driver version for the EMS Dr. W�nsche CPC-series interface boards 
CPC_VERSION=4.x
CPC_VERSION=3.x

###########################################################################
# 1a: target software version
# can be overwritten by commandline
version=
version=server

HOST = ipc2

HEADER="Horch"

SIM=SIM
SIM=XXX

# Used release tag for this software version
VERSION=3
REL=0
RELEASE=HORCH_$(VERSION)_$(REL)
PATCH=.0
HORCHVERSION=0x0300
HORCHMAXCLIENTS=1
HORCHMAXCLIENTS=10

CTAGS=elvtags
CTAGS=ctags
ifeq ($(OSTYPE), cygwin)
 CTAGS=C:/Programme/elvis2.1_4/ctags.exe
 CTAGS=C:/Programme/elvis/ctags.exe
 SHELL=/bin/sh
endif
ifeq ($(OSTYPE), cygwin32)
 CTAGS=/Programme/elvis/ctags.exe
 SHELL=/bin/sh
endif

# Directory to copy executable for test
HORCH_TESTDIR=C:/Programme/CANopen
ETHERCAN     = 194.173.33.69

BORLAND=//c/Borland/bcc55
BORLAND=/cygdrive/c/Borland/bcc55
BORLAND_D=c:/Borland/bcc55
BC_PATH=$(BORLAND)/BIN
BC_INC_PATH=$(BORLAND_D)/INCLUDE
BC_LIB_PATH=$(BORLAND_D)/LIB

CYG_PATH=/cygnus/cygwin-b20/H-i586-cygwin32/bin


# make ohne parameter nimmt den ersten Eintrag
dummy: all

ifeq "$(TARGET)" "LX_WIN_GCC"
######################################################################
CFLAGS	= -g -O3 -DTARGET_$(TARGET) -DUSG -mno-cygwin -mstrict-align\
	  -I ../include 		\
	  -I .
CFLAGS	= -g -DTARGET_$(TARGET) -DUSG -mno-cygwin \
	  -I.

OBJE=.o

TARGET_EXT=_g
OBJS	= horch.o  error_f.o  getopt.o can_lx.o version.o
endif

ifeq "$(TARGET)" "LX_WIN_BC"
######################################################################
ALIGNMENT=1

ifneq ($(OSTYPE), linux)
PATH=.:/bin:/usr/local/bin:$(CYG_PATH):\
/windows:/windows/command:$(BC_PATH)
endif

DRIVER_INCLUDES = -Idrivers	\
		  -Idrivers/can	\
		  -Idrivers/lx	\
		  -Idrivers/shar_inc	\
		  -Idrivers/shar_src	\
		  -I../can
		  
CC		= bcc32 -v -a$(ALIGNMENT)
CC		= bcc32 -a$(ALIGNMENT)
CPP		= 
LD		= tlink32
CFLAGS	= -O3 -DTARGET_$(TARGET) -DUSG \
	  -I ../include 		\
	  -I .
CFLAGS	= -O2 -DTARGET_$(TARGET) -DUSG -D__STDC__ \
	  -D_X86_ -DWIN32_LEAN_AND_MEAN \
	  -DCONFIG_MINI_HEADER=1 \
	  -I. $(DRIVER_INCLUDES) -I$(BC_INC_PATH) \
	  	-DCONFIG_EXTENDED_IDENTIFIER	\
		-DCONFIG_USE_TIMESTAMP \

#CFLAGS	+= -DDEBUGCODE
CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)

OBJE=.obj
TARGET_EXT=_lx
#OBJS	= horch$(OBJE)  error_f$(OBJE)  getopt$(OBJE) can_lx$(OBJE) \
#	version$(OBJE)
OBJS	= horch$(OBJE) co_win_bc$(OBJE) getopt$(OBJE) version$(OBJE) \
	  init_lx$(OBJE) can$(OBJE) cpu$(OBJE) board$(OBJE)\
	  canopen$(OBJE) \
	  filter/filter$(OBJE) \
	  socklib/socklib$(OBJE)


.SUFFIXES: .obj
.c.obj:
	$(CC) $(CFLAGS) -c $<

# LevelX specials
init_lx$(OBJE):	drivers/lx/init_lx.c
	$(CC) $(CFLAGS) -c $<

can$(OBJE):	drivers/lx/can.c
	$(CC) $(CFLAGS) -c $<

cpu$(OBJE):	drivers/lx/cpu.c 
	$(CC) $(CFLAGS) -c $<

board$(OBJE):	drivers/lx/board.c
	$(CC) $(CFLAGS) -c $<

canopen$(OBJE):	drivers/can/canopen.c
	$(CC) $(CFLAGS) -c $<

filter/filter$(OBJE):	filter/filter.c filter/filter.h
	$(CC) $(CFLAGS) -c $<
	mv filter$(OBJE) filter/
	
socklib/socklib$(OBJE):	socklib/socklib.c socklib/socklib.h
	$(CC) $(CFLAGS) -c $<
	mv socklib$(OBJE) socklib/
##
endif

ifeq "$(TARGET)" "CPC_LINUX"
######################################################################
OBJE=.o
CFLAGS = -g -Dlinux -Dunix 
#reduzieren auf TARGET_CPC_LINUX
CFLAGS += -DTARGET_CPC_LINUX
CFLAGS += -DCPC_LINUX
CFLAGS += -DTARGET_CPC_ECO

CFLAGS += -I.\
	  -Idrivers/can	\
	  -Idrivers/l2_cpcwin/cpc_linux	\
	  -Idrivers/cpcwin	\
	  -Idrivers/shar_inc	\
	  -Idrivers/shar_src	\
	  -Idrivers

CFLAGS +=  	-DCONFIG_MINI_HEADER=1	\
		-DCONFIG_USE_TIMESTAMP \
		-DCONFIG_EXTENDED_IDENTIFIER
CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)

LINK_LIB = drivers/l2_cpcwin/cpc_linux/cpclib_linux.o

TARGET_EXT= _cpcl
OBJS		= horch.o  canopen.o co_linux.o version$(OBJE) \
		  filter/filter$(OBJE) \
		  socklib/socklib$(OBJE)
DRIVER_OBJS     = can.o init_cpl.o linux.o

#.SUFFIXES: .o
#.c.o:
#	$(CC) $(CFLAGS) -c $<

filter/filter.o:	filter/filter.c filter/filter.h

socklib/socklib.o:	socklib/socklib.c socklib/socklib.h

linux$(OBJE):		drivers/cpcwin/linux.c
	$(CC) $(CFLAGS) -c $<

can$(OBJE):		drivers/cpcwin/can.c
	$(CC) $(CFLAGS) -c $<

init_cpl$(OBJE):	drivers/cpcwin/init_cpl.c
	$(CC) $(CFLAGS) -c $<

canopen$(OBJE):		drivers/can/canopen.c
	$(CC) $(CFLAGS) -c $<

endif


###########################################################################
# -v makes gcc very verbose
ifeq "$(TARGET)" "LINUX_ARM"
###########################################################################
# set PATH to the Crosscompiler
LOC  =	/opt/armtools
PATH =	/z2/0/0340:/usr/share/port/bin:/usr/local/bin:/bin:/usr/bin:$(LOC)/bin:$(LOC)/lib/gcc-lib/arm-elf/2.95.3

export PATH
CROSS   = arm-elf-
CC      = $(CROSS)gcc
CFLAGS  = -Os  -Dlinux -D__linux__ -Dunix -D__uClinux__ -DEMBED \
            	-DPLATFORM_ETHERCAN 

#-DCONFIG_ALIGNMENT=4   # is already defined in the driver sources

#CFLAGS	+= -DDEBUGCODE
#reduzieren auf TARGET_LINUX_ARM
CFLAGS	+= -DTARGET_LINUX_ARM
CFLAGS	+= -DTARGET_CPC_ECO 
CFLAGS	+= -DLINUX_ARM 

LDFLAGS =  -Wl,-elf2flt -Wl,--strip-all

EXEC = horch

OBJE=.o

CPCLIB        = 
ROOTDIR	      = /z2/0/0540/software
CPCLIB        = /z2/0/0540/arm/horch_test/cpclib.o
CPCLIB        = $(ROOTDIR)/user/cpclib/src/cpclib.o
CPCLIB        = drivers/l2_cpcwin/cpc_linux/cpclib_arm.o

DRIVERINC += 	-I.\
		-Idrivers$(VEREXT)/shar_inc \
		-Idrivers$(VEREXT)/shar_src \
		-Idrivers$(VEREXT)/cpcwin \
		-Idrivers/can \
		-Idrivers$(VEREXT)/ \
	 	-I$(ROOTDIR)/user/cpclib/include/ \
	 	-I$(ROOTDIR)/linux-2.4.x/drivers/can/ \
	 	-I$(ROOTDIR)/linux-2.4.x/cpclib/include/
#	  -Idrivers$(VEREXT)/l2_cpcwin/cpc_linux \

CFLAGS +=  	-DCONFIG_MINI_HEADER=1	\
		-DCONFIG_EXTENDED_IDENTIFIER \
		-DCONFIG_USE_TIMESTAMP \
		
#		-DCONFIG_DRIVER_TEST \
		

CFLAGS += $(DRIVERINC) $(INCDIR) $(DEFS)
		
CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)
#for internal test don't overwrite the linux m4d
TARGET_EXT=_arm

OBJS		= horch.o  canopen.o co_linux.o version$(OBJE) \
		  filter/filter$(OBJE) \
		  socklib/socklib$(OBJE)
DRIVER_OBJS     = can.o init_cpl.o linux.o led.o

filter/filter.o:	filter/filter.c filter/filter.h

socklib/socklib.o:	socklib/socklib.c socklib/socklib.h

#OBJS = horch.o version$(OBJE)
#DRIVER_OBJS =

VPATH = .:drivers/cpcwin/:drivers/can/

.SUFFIXES: .o
%.c.o:
	$(CC) $(CFLAGS) -c $<

linux$(OBJE):		drivers/cpcwin/linux.c
	$(CC) $(CFLAGS) -c $<
#can$(OBJE):		drivers/cpcwin/can.c
#	$(CC) $(CFLAGS) -c $<
#init_cpl$(OBJE):	drivers/cpcwin/init_cpl.c
#	$(CC) $(CFLAGS) -c $<
#canopen$(OBJE):		drivers/can/canopen.c
#	$(CC) $(CFLAGS) -c $<


drivers/l2_cpcwin/cpc_linux/cpclib.o: drivers/l2_cpcwin/cpc_linux/cpclib.c
	$(CC) $(CFLAGS) -c -o $@ $<

endif


ifeq "$(TARGET)" "CPC_ETHERCAN"
######################################################################
PATH = /bin:/usr/bin:/usr/local/bin:/usr/local/arm-uclinux-tools/bin

OBJE=.o

OBJFMT        = elf
OBJFMTD       = elfdebug
OBJFMT2FLT    = ldelf2flt
#LDFLAGS	      = -T $(NETARMROOT)/userapps/glibc_apps/rt_init/user.ld
LDFLAGS	      = -T rt_init/user.ld
LD            = arm-uclinux-ld
CC            = arm-uclinux-gcc



CFLAGS        = -g -O -Wall -mapcs-32 -mcpu=arm7tdmi -fpic -msingle-pic-base -mno-got -msoft-float -D__uclinux__

CFLAGS += -DTARGET_CPC_ECO -DTARGET_LINUX 
CFLAGS += -I.\
	  -Idrivers/can	\
	  -Idrivers/cpcwin	\
	  -Idrivers/shar_inc	\
	  -Idrivers \
-I/z2/0/0540/development/netlx/NETLx/userapps/glibc_apps/cpclib_arm/include/ \
		-I/z2/0/0540/development/netlx/NETLx/kernel/linux/arch/armnommu/drivers/can/


	  #-Idrivers/l2_cpcwin/cpc_linux	\



CFLAGS +=  	-DCONFIG_MINI_HEADER=1	\
		-DCONFIG_EXTENDED_IDENTIFIER
		#-DCONFIG_DRIVER_TEST

LINK_LIB = drivers/l2_cpcwin/cpc_linux/cpclib.o
LINK_LIB = cpclib.o

TARGET_EXT= _arm
OBJS		= horch.o  canopen.o co_linux.o version$(OBJE)
DRIVER_OBJS     = can.o init_cpl.o linux.o

.SUFFIXES: .o
.c.o:
	$(CC) $(CFLAGS) -c $<

linux$(OBJE):		drivers/cpcwin/linux.c
	$(CC) $(CFLAGS) -c $<
can$(OBJE):		drivers/cpcwin/can.c
	$(CC) $(CFLAGS) -c $<
init_cpl$(OBJE):	drivers/cpcwin/init_cpl.c
	$(CC) $(CFLAGS) -c $<
canopen$(OBJE):		drivers/can/canopen.c
	$(CC) $(CFLAGS) -c $<
endif

ifeq "$(TARGET)" "CPC_PP_WIN_BC"
######################################################################
CFLAGS =
TARGETX = CPC_WIN_BC
TARGET_SYSTEM =	PP
DRIVER_INCLUDES = -Idrivers/l2_cpcwin
LINK_LIB = drivers/l2_cpcwin/3d_cpcwin32.lib
endif
ifeq "$(TARGET)" "CPC_ECO_WIN_BC"
######################################################################
CFLAGS = 	-DTARGET_CPC_ECO \
		-DCONFIG_EXTENDED_IDENTIFIER \
		-DCONFIG_USE_TIMESTAMP \

#CFLAGS	+= -DCONFIG_DRIVER_TEST
#CFLAGS	+= -DDEBUGCODE

TARGETX = CPC_WIN_BC
TARGET_SYSTEM = ECO
DRIVER_INCLUDES = -Idrivers/l2_cpcwin/cpc_eco
ifeq "$(CPC_VERSION)" "4.x"
LINK_LIB = z:/2/0974/software/cpc/cpcwingb.lib
else
LINK_LIB = drivers/l2_cpcwin/cpc_eco/cpcwin_b.lib
endif
endif

ifeq "$(TARGETX)" "CPC_WIN_BC"
######################################################################
# fr�her alignment 8 - evtl beim CPC PP noch
# ALIGNMENT=8
# neuere Eco-Librarys benutzen Alignment 1
# CPC-Header setzen das korrekte Alignment.
# Daher hier nicht n�tig?
ifneq ($(OSTYPE), linux)
ALIGNMENT=8
PATH=.:/bin:/usr/local/bin:$(BIN_PATH):$(BC_PATH):$(CYG_PATH)
export PATH
endif

DRIVER_INCLUDES += -Idrivers	\
		  -Idrivers/can	\
		  -Idrivers/cpcwin	\
		  -Idrivers/shar_inc	\
		  -Idrivers/shar_src	

# -5 == Pentium
# -6 == Pentium Pro
CC		= bcc32 -v -a$(ALIGNMENT)
CC		= bcc32 -a$(ALIGNMENT) -5
CPP		= 
LD		= tlink32 -v
LD		= tlink32
#CFLAGS	= -O3 -DTARGET_$(TARGET) -DUSG \
#	  -I ../include 		\
#	  -I .
CFLAGS	+= -O2 -DTARGET_$(TARGETX) \
		-DUSG -D__STDC__ \
	  -D_X86_ -DWIN32_LEAN_AND_MEAN \
	  -DCONFIG_MINI_HEADER=1 \
	  -I. $(DRIVER_INCLUDES) -I$(BC_INC_PATH)

CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)
OBJE=.obj
TARGET_EXT=_cpc_$(TARGET_SYSTEM)
TARGET_EXT=_cpcw
#OBJS	= horch$(OBJE)  error_f$(OBJE)  getopt$(OBJE) can_lx$(OBJE) \
#	version$(OBJE)
OBJS	= horch$(OBJE) co_win_bc$(OBJE) getopt$(OBJE) version$(OBJE)
DRIVER_OBJS     = init_cpw$(OBJE) can$(OBJE) cpu$(OBJE) \
	  	  canopen$(OBJE) \
		  filter/filter$(OBJE) \
		  socklib/socklib$(OBJE)

#filter/filter$(OBJE):	filter/filter.c filter/filter.h

.SUFFIXES: .obj
.c.obj:
	$(CC) $(CFLAGS) -c $<

# cpc specials
init_cpw$(OBJE):	drivers/cpcwin/init_cpw.c
	$(CC) $(CFLAGS) -c $<

can$(OBJE):	drivers/cpcwin/can.c
	$(CC) $(CFLAGS) -c $<

cpu$(OBJE):	drivers/cpcwin/cpu.c 
	$(CC) $(CFLAGS) -c $<

canopen$(OBJE):	drivers/can/canopen.c
	$(CC) $(CFLAGS) -c $<

filter/filter$(OBJE):	filter/filter.c filter/filter.h
	$(CC) $(CFLAGS) -c $<
	mv filter$(OBJE) filter/

socklib/socklib$(OBJE):	socklib/socklib.c socklib/socklib.h
	$(CC) $(CFLAGS) -c $<
	mv socklib$(OBJE) socklib/

##
endif

ifeq "$(TARGET)" "AC2P_WIN_BC"
######################################################################
TARGET = AC2_WIN_BC
TARGET_SYSTEM = PCI
LINK_LIB = drivers/l2_ac2/pci/canacpci.lib
endif
ifeq "$(TARGET)" "AC2I_WIN_BC"
######################################################################
TARGET = AC2_WIN_BC
TARGET_SYSTEM = ISA
LINK_LIB = drivers/l2_ac2/isa/canac.lib
endif

ifeq "$(TARGET)" "AC2_WIN_BC"
######################################################################
ALIGNMENT=1

ifneq ($(OSTYPE), linux)
PATH=.:/bin:/usr/local/bin:$(CYG_PATH):\
/windows:/windows/command:$(BC_PATH)
endif

DRIVER_INCLUDES = -Idrivers	\
		  -Idrivers/can	\
		  -Idrivers/ac2	\
		  -Idrivers/shar_inc	\
		  -I../can
		  
CC		= bcc32 -a$(ALIGNMENT)
CC		= bcc32 -v -a$(ALIGNMENT)
CPP		= 
LD		= tlink32
CFLAGS	= -O3 -DTARGET_$(TARGET) -DUSG \
	  -I ../include 		\
	  -I .
CFLAGS	= -O2 -DTARGET_$(TARGET) -DUSG -D__STDC__ \
	  -D_X86_ -DWIN32_LEAN_AND_MEAN \
	  -DCONFIG_MINI_HEADER=1 \
	  -I. $(DRIVER_INCLUDES) -I$(BC_INC_PATH)

CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)
OBJE=.obj
TARGET_EXT=_ac2_$(TARGET_SYSTEM)
#OBJS	= horch$(OBJE)  error_f$(OBJE)  getopt$(OBJE) can_lx$(OBJE) \
#	version$(OBJE)
OBJS	= horch$(OBJE) co_win_bc$(OBJE) getopt$(OBJE) version$(OBJE)
DRIVER_OBJS     = init_ac2$(OBJE) can$(OBJE) cpu$(OBJE) \
	  	  canopen$(OBJE)

.SUFFIXES: .obj
.c.obj:
	$(CC) $(CFLAGS) -c $<

# AC2 specials
init_ac2$(OBJE):	drivers/ac2/init_ac2.c
	$(CC) $(CFLAGS) -c $<

can$(OBJE):	drivers/ac2/can.c
	$(CC) $(CFLAGS) -c $<

cpu$(OBJE):	drivers/ac2/cpu.c 
	$(CC) $(CFLAGS) -c $<

canopen$(OBJE):	drivers/can/canopen.c
	$(CC) $(CFLAGS) -c $<
##
endif

ifeq "$(TARGET)" "LINUX"
######################################################################
OBJE=.o
# used driver, can4linux is default, else call make DRV=socketcan
DRV = can4linux

DRIVER_INCLUDE= -I /usr/local/src/can4linux/src
DRIVER_INCLUDE= -I /usr/src/can4linux/
#CFLAGS	= -g -O3 -DTARGET_$(TARGET) -D$(SIM) \
	  -I . 				\
	  $(DRIVER_INCLUDE)	\

# use -pg for profiling with gprof
CFLAGS	+= -g -DTARGET_$(TARGET) -D$(SIM) \
	  -I . 				\
	  $(DRIVER_INCLUDE)	\

CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS) \
	   #-DUSER_TERMIOS

# CanSja1000Status_par_t statt CanStatusPar_t
#CFLAGS += -DCAN4LINUX_OLD

# Filter Debugcode aktivieren
#CFLAGS += -DDEBUGCODE
	  

OBJ_DIR = obj
#TARGET_NAME = slave401_$(shell $(CC) -dumpmachine)_$(DRV)




OBJS		= horch.o version$(OBJE) \
		  filter/filter$(OBJE) \
		  socklib/socklib$(OBJE)
ifeq ($(DRV), socketcan)
DRIVER_OBJS = linux_socketcan.o
TARGET_EXT =_socketcan
CFLAGS+=-DSOCKETCAN
else
DRIVER_OBJS = linux.o
TARGET_EXT =_can4linux
CFLAGS+=-DCAN4LINUX
endif

filter/filter.o:	filter/filter.c filter/filter.h

socklib/socklib.o:	socklib/socklib.c socklib/socklib.h

endif

ifeq "$(TARGET)" "LINUX_COLDFIRE"
######################################################################
LOC  =	/opt/armtools
PATH =	/z2/0/0340:/usr/share/port/bin:/usr/local/bin:/bin:/usr/bin:$(LOC)/bin:$(LOC)/lib/gcc-lib/arm-elf/2.95.3

LOC  = /home/oertel/pakete/cf_CC/uClinux-dist/

export PATH
CROSS   = m68k-elf-
CC      = $(CROSS)gcc

#UCINCLUDE = \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/lib/uClibc/include \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/lib/libm \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/lib/libcrypt_old \
	-I/home/oertel/pakete/cf_CC/uClinux-dist \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/linux-2.4.x/include \



UCBASE = /home/oertel/pakete/uClinux-dist-20030909-SSV20040610
# Contem Controls
UCLIBBASE = /home/oertel/pakete/uClinux-dist-20030909-SSV20040610/lib
# SSV IGW/900
UCLIBBASE = $(UCBASE)/lib
UCLIBS = \
	-L$(UCLIBBASE)/uClibc/. \
	-L$(UCLIBBASE)/uClibc/lib \
	-L$(UCLIBBASE)/libm \
	-L$(UCLIBBASE)/libnet \
	-L$(UCLIBBASE)/libdes \
	-L$(UCLIBBASE)/libaes \
	-L$(UCLIBBASE)/libpcap \
	-L$(UCLIBBASE)/libssl \
	-L$(UCLIBBASE)/libcrypt_old \
	-L$(UCLIBBASE)/libsnapgear++ \
	-L$(UCLIBBASE)/libsnapgear \
	-L$(UCLIBBASE)/zlib \

LDFLAGS =  -Wl,-elf2flt -Wl,--strip-all
LDFLAGS = $(CFLAGS) -Wl,-elf2flt -Wl,-move-rodata -nostartfiles  \
	$(UCLIBBASE)/uClibc/lib/crt0.o \
	$(UCLIBS)

	#-o can_send can_send.o -lc

OBJE=.o
#DRIVER_INCLUDE= -I /usr/local/src/can4linux/src
#DRIVER_INCLUDE= -I /usr/src/can4linux/src
DRIVER_INCLUDE= -I /z2/0/0530/software/can4linux/src
DRIVER_INCLUDE= -I $(UCBASE)/user/can4linux \
    -I drivers/ssv \
    -I /home/oertel/pakete/uClinux-dist-20030909-SSV20040610/vendors/SSV/src/ssvhwa-demos

CFLAGS  =  -m5307 -DCONFIG_COLDFIRE -DTARGET_$(TARGET) \
		-Dlinux -D__linux__ -Dunix -D__uClinux__ -DEMBED \
		-Os -fomit-frame-pointer \
		-fno-builtin -msep-data \
		-I. $(DRIVER_INCLUDE) \
		$(UCINCLUDE)
# CFLAGS += -g


CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)

TARGET_EXT = _cf
OBJS		= horch.o  linux.o version$(OBJE) \
		  filter/filter$(OBJE) \
		  socklib/socklib$(OBJE) 
		  
DRIVER_OBJS     = drivers/ssv/led$(OBJE) \
		  drivers/ssv/ssvhwa$(OBJE)

filter/filter.o:	filter/filter.c filter/filter.h

socklib/socklib.o:	socklib/socklib.c socklib/socklib.h


endif

ifeq "$(TARGET)" "LINUX_BF"
######################################################################
LOC=/opt/uClinux/bfin-uclinux

PATH =	/z2/0/0340:/usr/share/port/bin:/usr/local/bin:/bin:/usr/bin:$(LOC)/bin:

#$(LOC)/lib/gcc-lib/arm-elf/2.95.3

#LOC  = /home/oertel/pakete/cf_CC/uClinux-dist/

export PATH
CROSS   = bfin-uclinux-
#CC      = /opt/uClinux/bfin-uclinux/bin/$(CROSS)gcc
CC      = $(CROSS)gcc

UCINCLUDE = \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/lib/uClibc/include \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/lib/libm \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/lib/libcrypt_old \
	-I/home/oertel/pakete/cf_CC/uClinux-dist \
	-I/home/oertel/pakete/cf_CC/uClinux-dist/linux-2.4.x/include \


UCLIBBASE = /home/oertel/pakete/uclinux533-work/uClinux-dist/lib
#UCLIBS = \
	-L$(UCLIBBASE)/uClibc/. \
	-L$(UCLIBBASE)/uClibc/lib \
	-L$(UCLIBBASE)/libm \
	-L$(UCLIBBASE)/libnet \
	-L$(UCLIBBASE)/libdes \
	-L$(UCLIBBASE)/libaes \
	-L$(UCLIBBASE)/libpcap \
	-L$(UCLIBBASE)/libssl \
	-L$(UCLIBBASE)/libcrypt_old \
	-L$(UCLIBBASE)/libsnapgear++ \
	-L$(UCLIBBASE)/libsnapgear \
	-L$(UCLIBBASE)/zlib \

LDFLAGS =  -Wl,-elf2flt -Wl,--strip-all
LDFLAGS = -v $(CFLAGS) -Wl,-elf2flt -Wl,-move-rodata -nostartfiles  \
	$(UCLIBBASE)/uClibc/lib/crt0.o \
	$(UCLIBS)

OBJE=.o
DRIVER_INCLUDE= -I /z2/0/0530/software/can4linuxbf
DRIVER_INCLUDE= -I /z2/0/0530/software/can4linux

CFLAGS  =  	-v -DCONFIG_BF -DTARGET_$(TARGET) \
		-Dlinux -D__linux__ -Dunix -D__uClinux__ -DEMBED \
		-g -Os -Wall -fomit-frame-pointer \
		-fno-builtin \
		-I. $(DRIVER_INCLUDE) \
		$(UCINCLUDE)


CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)

TARGET_EXT = _bf
OBJS		= horch.o  linux.o version$(OBJE) \
		  filter/filter$(OBJE) \
		  socklib/socklib$(OBJE) 
		  
#DRIVER_OBJS     = drivers/ssv/led$(OBJE) \
		  drivers/ssv/ssvhwa$(OBJE)

filter/filter.o:	filter/filter.c filter/filter.h

socklib/socklib.o:	socklib/socklib.c socklib/socklib.h

endif

ifeq "$(TARGET)" "LINUX_PPC"
######################################################################
OBJE=.o
DRIVER_INCLUDE= -I ../Can/include

CC	= powerpc-linux-gcc
CFLAGS	= -g -O3 -DTARGET_$(TARGET) -D$(SIM) \
	  -I . 				\
	  $(DRIVER_INCLUDE)

CFLAGS	+= -DCONFIG_HORCH=$(HORCHVERSION) -DHORCH_MAX_CLIENTS=$(HORCHMAXCLIENTS)
TARGET_EXT =
OBJS		= horch.o  linux.o version$(OBJE)
DRIVER_OBJS     = 

endif


ifeq "$(TARGET)" "CYGWIN"
######################################################################
CFLAGS	= -g -O3 -DTARGET_$(TARGET)	\
	  -I ../include 		\
	  -I .

TARGET_EXT=_c
OBJS	= horch.o  error_f.o  can_util.o
endif

ifeq "$(TARGET)" "__W32__"
######################################################################
CFLAGS	= -g -O3 -DTARGET_$(TARGET) -mno-cygwin\
	  -I ../include 		\
	  -I .

TARGET_EXT=
OBJS	= horch.o  error_f.o  can_util.o getopt.o
endif

ifeq "$(TARGET)" "DOS16"
######################################################################
CC	= bcc
OBJE=.obj

# -ms
CFLAGS	=  -2 -O2  -DTARGET_$(TARGET) -A \
	  -I../include 		\
	  -I.

TARGET_EXT=_d
OBJS	=  horch$(OBJE)  error_f$(OBJE)  can_util$(OBJE) getopt$(OBJE)

.SUFFIXES: .obj
.c.obj:
	$(CC) $(CFLAGS)  $<

endif

ifeq "$(TARGET)" "IPC"
######################################################################
CCROOT		= //C/bc5

ifneq ($(OSTYPE), linux)
PATH=.:/bin:/usr/local/bin:/cygnus/cygwin-b20/H-i586-cygwin32/bin:\
/windows:/windows/command:$(CCROOT)/bin
endif

# -ml
CC		= bcc -v
CC		= bcc
CPP		= 
LD		= tlink \/Oc
AS		= 
MODEL=l
OBJE=.obj
LIBIPC=		Z:/0/0540/development/software/src/libipc-0.03/src/libipc.lib
LIBIPC=		libipc.lib

# -N -- switch Stack stack on, be careful if using this
CFLAGS	=  -1 -a1 -m$(MODEL) -DTARGET_$(TARGET) -DUSG \
	  -I. -Ican \
	  -IZ:/0/0540/development/software/src/libipc-0.03/src/ipc\
	  -IZ:/0/0540/development/software/src/libipc-0.03/src

TARGET_EXT =
OBJS		= horch$(OBJE)  ipc$(OBJE) \
		  version$(OBJE)
		  #socket$(OBJE)\
		  # getopt$(OBJE) setpio$(OBJE) 

LIBS		= $(LIBIPC) noeh$(MODEL).lib

.SUFFIXES: .obj
.c.obj:
	$(CC) $(CFLAGS)  -c $<

endif


######################################################################
######################################################################
#
# Link all together
#
#
#
# Multiclient
MC_EXT=
ifneq "$(HORCHMAXCLIENTS)" "1"
# glpl horch is always multiclient
# MC_EXT=_mc
endif

all:	horch$(TARGET_EXT)$(MC_EXT)

#all:	horch_sk
## make all_versions - create 1Client und Multi Client Version
all_versions:
	make HORCHMAXCLIENTS=1 TARGET=$(TARGET) clean
	make HORCHMAXCLIENTS=1 TARGET=$(TARGET) all
	make HORCHMAXCLIENTS=10 TARGET=$(TARGET) clean
	make HORCHMAXCLIENTS=10 TARGET=$(TARGET) all

error_f$(OBJE):	../appl/error_f.c
	$(CC) $(CFLAGS) -c -I../include $<

$(OBJS):	horch.h

# ###########################################################################
#
# This is the linking section
# What happens here is depending of the compiler and target architecture
# ###########################################################################
horch$(TARGET_EXT)$(MC_EXT): 	$(OBJS) $(DRIVER_OBJS)


#
# -------------------------------------------------------------------------
ifeq "$(TARGET)" "DOS16"
	$(CC) -tD -e$@ $(OBJS)
endif

# Following some Borland Compiler projects for Win32
# -------------------------------------------------------------------------
ifeq "$(TARGET)" "LX_WIN_BC"
	$(CC) $(CFLAGS) -e$@ -L$(BC_LIB_PATH) $(OBJS) 
endif
# -------------------------------------------------------------------------
ifeq "$(TARGET)" "AC2_WIN_BC"
	$(CC) $(CFLAGS) -e$@ -L$(BC_LIB_PATH) $(OBJS) $(DRIVER_OBJS) $(LINK_LIB)
endif
# -------------------------------------------------------------------------
ifeq "$(TARGETX)" "CPC_WIN_BC"
	$(CC) $(CFLAGS) -e$@ -L$(BC_LIB_PATH) $(OBJS) $(DRIVER_OBJS) $(LINK_LIB)
endif

# Linux using hardware supported by the CPC driver software of EMS
# -------------------------------------------------------------------------
ifeq "$(TARGET)" "CPC_LINUX"
	$(CC) $(CFLAGS) -o$@ $(OBJS) $(DRIVER_OBJS) $(LINK_LIB)
endif

# uClinux EtherCAN -- Than its LINUX _AND_  __uClinux__
# -------------------------------------------------------------------------
ifeq "$(TARGET)" "LINUX"
 ifeq "$(CONFIG_PLATFORM_ETHERCAN_CI)" "y"
	@echo "==> Linking horch for LINUX embedded uClinux"
	$(CC) $(LDFLAGS) $(CFLAGS) -o$@ $(OBJS) $(DRIVER_OBJS) \
		$(LDLIBS$(LDLIBS-$(@)))
 else
  #  two versions for linux
  #    horch  - dynamically linked
  #    horchs - statically linked
	@echo "==> Linking horch for Desktop LINUX"
	$(CC) $(CFLAGS) -o$@ $(OBJS) $(DRIVER_OBJS)
	@echo "skip static linking"
	#$(CC) $(CFLAGS) -static -o$@s $(OBJS)
 endif
endif
# ------------------------------------------------------------------------
ifeq "$(TARGET)" "CPC_ETHERCAN"
	#$(CC) $(CFLAGS) -o$@ $(OBJS) $(DRIVER_OBJS) $(LIB_CPC)
	$(CC) $(CFLAGS) $(LDFLAGS) -o$@ $(OBJS) $(DRIVER_OBJS) $(LINK_LIB) \
			$(LIBS) $(NETLIBS)
	echo "Used libs: $(LIBS) $(NETLIBS)"
	mv  $@ $@.elf
	$(OBJFMT2FLT) -s 40960 -o $@ $@.elf

endif

ifeq "$(TARGET)" "LINUX_ARM"
	$(CC) $(LDFLAGS) -o $@  \
	$(OBJS) $(DRIVER_OBJS) $(CPCLIB) $(LDLIBS)
	chmod 755 $@
#DRIVER_OBJS     = init_cpl$(OBJE) linux$(OBJE) can$(OBJE)
endif

ifeq "$(TARGET)" "LINUX_COLDFIRE"
	$(CC) -o $@ $(LDFLAGS) \
	$(OBJS) $(DRIVER_OBJS) $(LDLIBS) -lc
	chmod 755 $@
endif

ifeq "$(TARGET)" "LINUX_BF"
	$(CC) -o $@ $(LDFLAGS) \
	$(OBJS) $(DRIVER_OBJS) $(LDLIBS) -lc
	chmod 755 $@
endif

ifeq "$(TARGET)" "IPC"
	$(CC) -e$@ -ml -ls -ll $(OBJS) can/can.obj $(LIBS)
endif
ifeq "$(TARGET)" "LINUX_PPC"
	$(CC) $(CFLAGS) -o$@ $(OBJS)
	cp horch ../../../root/ppc/usr/bin
endif

clean:
ifeq "$(OBJE)" ""
	-rm horch$(TARGET_EXT) horch$(TARGET_EXT).exe tags \
	version.c
else
	-rm $(OBJS) $(DRIVER_OBJS) \
	    horch$(TARGET_EXT)$(MC_EXT) \
	    horch$(TARGET_EXT)$(MC_EXT).exe \
	    tags \
	    version.c
endif

#############################################################################
## make manpage - manpage is created using Doxygen
##                at the project level ../man
manpage: horch.c
	doxygen

## make release
.PHONY:release
release:

#############################################################################
# put		--------------- transfer to EtherCAN with ftp  -------------
.PHONY:put
put:
	cp horch$(TARGET_EXT) h
	./put $(ETHERCAN) h
	
.PHONY:put_mc
put_mc:
	cp horch$(TARGET_EXT)_mc h
	./put $(ETHERCAN) h

#############################################################################
## make ctags - create tag-file

SOCKLIBSRC=	socklib/socklib.c socklib/socklib.h
FILTERSRC=	filter/filter.c filter.h

.PHONY:ctags
ctags:
ifeq "$(TARGET)" "LX_WIN_GCC"
	$(CTAGS) -stv horch.c can_lx.c *.h $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "LX_WIN_BC"
	$(CTAGS) -stv horch.c can_lx.c *.h $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "LINUX"
	$(CTAGS) horch.c *.h $(DRIVER_OBJS:.o=.c) $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "LINUX_COLDFIRE"
	$(CTAGS) -tvs horch.c *.h linux.c $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "LINUX_BF"
	$(CTAGS) -tvs horch.c *.h linux.c $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "CPC_LINUX"
	$(CTAGS) -tvs horch.c co_linux.c *.h drivers/can/* $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "CPC_ETHERCAN"
	$(CTAGS) -tvs horch.c co_linux.c *.h drivers/can/* \
	drivers/cpcwin/* $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "LINUX_ARM"
	$(CTAGS) -tvs horch.c co_linux.c *.h drivers/can/* \
	drivers/cpcwin/* $(SOCKLIBSRC) $(FILTERSRC)
endif
ifeq "$(TARGET)" "IPC"
	$(CTAGS) -tvs horch.c ipc.c can/*.c *.h $(FILTERSRC)
endif

# save	  --------------- transfer to save place  ------------------
.PHONY:save
save:
	(cd ..; tar zcvf horch.tgz horch)
	(cd ..; cp horch.tgz //a )

############################################################################
#              V e r s i o n  C o n t r o l
#
#
############################################################################
##
## CVS
## make commit - commit changes of all files to the cvs repository
commit:
ifneq ($(OSTYPE), linux)
	@echo "Please use 'make commit' from Linux!"
else	
	cvs commit -F commitfile
endif

## make tag - tag all files in the current module
tag:
ifneq ($(OSTYPE), linux)
	@echo "Please use 'make tag' from Linux!"
else	
	cvs tag $(RELEASE)
endif

version.c:	Makefile
	@echo "/* version file for horch, created by make */"   > $@
	@echo "char const horch_revision[] = \"$(VERSION).$(REL)$(PATCH)\";" >> $@


ipc.asm:	ipc.c
	$(CC) $(CFLAGS) -e$@ -ml -S $<



############################################################################
ZIP_FILE = horch_ems.zip
##
## Create archives for EMS
## make ems - create a zip-file for W�nsche CPC PCI card for Linux
.PHONY:ems
ems:
	# archive relative to software
	-rm ../$(ZIP_FILE)
	# Makefile
	(cd ..;zip -9 $(ZIP_FILE) software/Makefile)
	(cd ..;zip -9 $(ZIP_FILE) README.ems)
	# horch source
	(cd ..;zip -9 $(ZIP_FILE) software/co_linux.c)
	(cd ..;zip -9 $(ZIP_FILE) software/horch.c)
	(cd ..;zip -9 $(ZIP_FILE) software/horch.h)
	(cd ..;zip -9 $(ZIP_FILE) software/can.h)
	# drivers
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/can/*.c)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/can/*.h)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/cpcwin/can.c)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/cpcwin/linux.c)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/cpcwin/init_cpl.c)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/cpcwin/t_linux.h)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/l2_cpcwin/cpc_linux/*.h)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/l2_cpcwin/cpc_linux/*.o)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/target.h)
	# shar sources
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/shar_src/cdriver.c)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/shar_src/win_drv.c)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/shar_inc/cdriver.h)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/shar_inc/win_drv.h)
	(cd ..;zip -9 $(ZIP_FILE) software/drivers/shar_inc/82c200.h)

############################################################################
TAR_FILE = horch_ems.tgz
## make ems_tar - create a tar-file for W�nsche CPC Ethercan-ARM
.PHONY:ems_tar
ems_tar:
	(cd ..; tar -chzf $(TAR_FILE) \
		horch/drivers/can/cal_type.h \
		horch/drivers/can/canopen.c \
		horch/drivers/can/canopen.h \
		horch/drivers/cpcwin/Makefile \
		horch/drivers/cpcwin/t_linux.h \
		horch/drivers/cpcwin/can.c \
		horch/drivers/cpcwin/cpu.c \
		horch/drivers/cpcwin/init_cpl.c \
		horch/drivers/cpcwin/linux.c \
		horch/drivers/cpcwin/t_cpcwin.h \
		horch/drivers/target.h \
		horch/drivers/shar_inc/cdriver.h \
		horch/drivers/shar_inc/82c200.h \
		horch/drivers/shar_inc/win_drv.h \
		horch/drivers/shar_src/cdriver.c \
		horch/drivers/shar_src/win_drv.c \
		horch/co_linux.c \
		horch/getopt.c \
		horch/horch.c \
		horch/horch.h \
		horch/version.c \
		horch/Makefile \
	)


############################################################################
.PHONY:help
help:
	@echo	"make        - create horch"
	@echo	"make clean  - remove temporary files and horch binary"
	@echo	"----------------------------------------"
	@echo   ""
ifneq ($(OSTYPE), linux)
	@echo 	"You have started from Windows"
endif	
	@echo   ""
	@echo   "additonal helps:"
	@echo   "----------------"
	@echo   "TARGET=$(TARGET)" 
ifeq ($(TARGET), LINUX)
	@echo 	"  DRV=$(DRV)"
	@echo 	"  select make DRV=can4linux or DRV=socketcan"
endif	
	@echo   "RELEASE=$(RELEASE) PATCH=$(PATCH)"
	@echo   ""
	@sed -n '/^##/s/^## //p' Makefile 

# f�r EMS - EtherCAN Filesystem
romfs:
