# horch
horch is a simple TCP/IP server application which has access to a CAN device driver.
On Linux the two common used CAN drivers **can4linux** or **SocketCAN**
can be used to acess the CAN bus.
A client connecting to this server can receive CAN frames and send CAN frames.

compile use wether

$make DRV=can4linux
or
$ make DRV=socketcan

Call horch with option -h to get the usage information.
can4liux is a character device driver. Once it is laoded as module, it can be used immediately.
User applications can simple set the CAN bitrate with -b bitrate, e.g. -b 250 
Or predefine the bitrate in /proc/sys/dev/Can/Baud

Remark for socketcan

You can check, if a socketcan is installed and the can device is available 
$ ip link show

To configure the CAN interface with a bitrate and activate the network

sudo ip link set can0 up type can bitrate 250000 restart-ms 100

and check after with
$ ip -details link  show can0
